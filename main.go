package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"time"

	"gitlab.com/gl-infra/oncall-robot-assistant/config"
	oncall "gitlab.com/gl-infra/oncall-robot-assistant/oncall"
)

const settingsFname = ".oncall-settings.yaml"

func main() {
	var filepath string

	if _, err := os.Stat(path.Join(".", settingsFname)); err == nil {
		filepath = path.Join(".", settingsFname)
	}

	if _, err := os.Stat(path.Join(os.Getenv("HOME"), settingsFname)); err == nil {
		filepath = path.Join(os.Getenv("HOME"), settingsFname)
	}

	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		log.Fatal(fmt.Sprintf("ERROR: Unable to locate %s", settingsFname))
	}

	cfgFile := flag.String("config", filepath, "the configuration file")
	debug := flag.Bool("debug", false, "print the issue instead of creating it")
	flag.Parse()
	config, err := config.ReadConfig(*cfgFile, *debug)
	if err != nil {
		log.Fatalln(err)
	}
	if config.Projects.Infrastructure.ID == 0 {
		log.Fatalln("ERROR: It looks like you have an old settings file, please update using oncall-settings-example.yaml")
	}

	if config.Projects.Production.ID == 0 {
		log.Fatalln("ERROR: It looks like you have an old settings file, you are missing the production project ID. please update using oncall-settings-example.yaml")
	}

	opts := oncall.ReportOptions{}
	report := oncall.NewReport(config, opts)
	title := "OnCall report for period: " +
		time.Now().UTC().AddDate(0, 0, -7).Format("2006-01-02") +
		" - " + time.Now().UTC().Format("2006-01-02")
	issue := report.CreateReportIssue(title)
	log.Println("Created", issue)
}
