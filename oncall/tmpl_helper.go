package oncall

// WeeklyOpsGraph weekly ops grpah
type WeeklyOpsGraph struct {
	Name string
	URL  string
}

// TeamMember info for a team member
type TeamMember struct {
	Schedule string
	User     string
}

// IssueStats for report issues
type IssueStats struct {
	Count            int
	OnCall           int
	AccessRequest    int
	CorrectiveAction int
	Critical         int
	Outage           int
	Change           int
	Incident         int
}

// Issue for oncall labeled issues
type Issue struct {
	Summary        string
	URL            string
	CreatedAt      string
	Assignee       string
	SeverityLabels []string
	ServiceLabels  []string
}

// Incident for PD incident
type Incident struct {
	Summary   string
	URL       string
	CreatedAt string
}

//TemplateData for report generation
type TemplateData struct {
	WeeklyOpsGraphs         []WeeklyOpsGraph
	TeamMembers             []TeamMember
	Incidents               []Incident
	OpenOnCallIssues        []Issue
	OpenChangeIssues        []Issue
	OpenIncidentIssues      []Issue
	CriticalIssues          []Issue
	ChangeIssues            []Issue
	CorrectiveActionIssues  []Issue
	IncidentIssues          []Issue
	OutageIssues            []Issue
	IncidentCount           int
	IssuesOpenAll           IssueStats
	IssuesOpenedDuringShift IssueStats
}
