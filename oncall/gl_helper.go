package oncall

import (
	"fmt"
	"log"
	"strings"
	"time"

	gitlab "github.com/xanzy/go-gitlab"
)

// GitLabHelper api helper
type GitLabHelper struct {
	client        *gitlab.Client
	apiToken      string
	projectID     int
	projectIDProd int
	dayOffset     int
}

// GitLabHelperOptions options for the api helper
type GitLabHelperOptions struct {
	APIToken      string
	ProjectID     int
	ProjectIDProd int
	DayOffset     int
}

// NewGitLabHelper creates a new api helper
func NewGitLabHelper(opts GitLabHelperOptions) *GitLabHelper {
	r := GitLabHelper{
		client:        newGitLabClient(opts.APIToken),
		apiToken:      opts.APIToken,
		projectID:     opts.ProjectID,
		projectIDProd: opts.ProjectIDProd,
		dayOffset:     opts.DayOffset,
	}
	return &r
}

func newGitLabClient(apiToken string) *gitlab.Client {
	return gitlab.NewClient(nil, apiToken)
}

// GetMergeRequestsByProject get merge requests
func (g *GitLabHelper) GetMergeRequestsByProject() []*gitlab.MergeRequest {
	opts := gitlab.ListProjectMergeRequestsOptions{}
	dayInterval := time.Now().UTC().AddDate(0, 0, -g.dayOffset)
	opts.CreatedAfter = &dayInterval

	if mrs, _, err := g.client.MergeRequests.ListProjectMergeRequests(g.projectID, &opts); err != nil {
		panic(err)
	} else {
		return mrs
	}

}

// CreateIssue creates an issue
func (g *GitLabHelper) CreateIssue(title string, desc string, projID int) *gitlab.Issue {
	opts := gitlab.CreateIssueOptions{}
	opts.Title = &title
	opts.Description = &desc
	issue, _, err := g.client.Issues.CreateIssue(projID, &opts)
	if err != nil {
		log.Fatalf("Unable to create issue in GitLab project %d: %s", projID, err.Error())
	}
	return issue
}

// GetIssuesOpenedDuringShift gets opened issues during shift
func (g *GitLabHelper) GetIssuesOpenedDuringShift() []*gitlab.Issue {
	var opts gitlab.ListProjectIssuesOptions
	opts.ListOptions.PerPage = 100
	dayInterval := time.Now().UTC().AddDate(0, 0, -g.dayOffset)
	opts.CreatedAfter = &dayInterval
	allIssues := make([]*gitlab.Issue, 0)
	for {
		issues, r, err := g.client.Issues.ListProjectIssues(g.projectID, &opts)
		if err != nil {
			panic(err)
		}
		for _, issue := range issues {
			allIssues = append(allIssues, issue)
		}
		if r.NextPage == 0 {
			break
		}
		log.Println("Setting next page to ", r.NextPage)
		opts.ListOptions.Page = r.NextPage
	}
	return allIssues
}

// GetIssuesOpenedDuringShiftProd gets opened issues during shift for production issues
func (g *GitLabHelper) GetIssuesOpenedDuringShiftProd() []*gitlab.Issue {
	var opts gitlab.ListProjectIssuesOptions
	opts.ListOptions.PerPage = 100
	dayInterval := time.Now().UTC().AddDate(0, 0, -g.dayOffset)
	opts.CreatedAfter = &dayInterval
	allIssues := make([]*gitlab.Issue, 0)
	for {
		issues, r, err := g.client.Issues.ListProjectIssues(g.projectIDProd, &opts)
		if err != nil {
			panic(err)
		}
		for _, issue := range issues {
			allIssues = append(allIssues, issue)
		}
		if r.NextPage == 0 {
			break
		}
		log.Println("Setting next page to ", r.NextPage)
		opts.ListOptions.Page = r.NextPage
	}
	return allIssues
}

// GetIssuesOpenAll gets all open issues
func (g *GitLabHelper) GetIssuesOpenAll() []*gitlab.Issue {
	var opts gitlab.ListProjectIssuesOptions
	opts.ListOptions.PerPage = 100
	state := "opened"
	opts.State = &state
	allIssues := make([]*gitlab.Issue, 0)
	for {
		issues, r, err := g.client.Issues.ListProjectIssues(g.projectID, &opts)
		if err != nil {
			panic(err)
		}
		for _, issue := range issues {
			allIssues = append(allIssues, issue)
		}
		if r.NextPage == 0 {
			break
		}
		log.Println("Getting next page from response", r.NextPage)
		opts.ListOptions.Page = r.NextPage
	}
	return allIssues
}

// GetIssuesOpenAllProd gets all open issues in production
func (g *GitLabHelper) GetIssuesOpenAllProd() []*gitlab.Issue {
	var opts gitlab.ListProjectIssuesOptions
	opts.ListOptions.PerPage = 100
	state := "opened"
	opts.State = &state
	allIssues := make([]*gitlab.Issue, 0)
	for {
		issues, r, err := g.client.Issues.ListProjectIssues(g.projectIDProd, &opts)
		if err != nil {
			panic(err)
		}
		for _, issue := range issues {
			allIssues = append(allIssues, issue)
		}
		if r.NextPage == 0 {
			break
		}
		log.Println("Getting next page from response", r.NextPage)
		opts.ListOptions.Page = r.NextPage
	}
	return allIssues
}

// UploadFile uploads a file to a project
func (g *GitLabHelper) UploadFile(projectID int, fname string) *gitlab.ProjectFile {
	pf, _, err := g.client.Projects.UploadFile(projectID, fname)
	if err != nil {
		log.Fatalf("Unable to upload graphs from file %s to project id %d : %s", fname, projectID, err.Error())
	}
	return pf
}

func filterSeverityLabels(labels []string) (filteredLabels []string) {
	// severityTest := func(s string) bool { return strings.HasPrefix(s, "S") && len(s) <= 2 }
	for _, l := range labels {
		if strings.HasPrefix(l, "S") && len(l) <= 2 {
			filteredLabels = append(filteredLabels, l)
		}
	}
	return
}

func filterServiceLabels(labels []string) (filteredLabels []string) {
	// severityTest := func(s string) bool { return strings.HasPrefix(s, "S") && len(s) <= 2 }
	for _, l := range labels {
		if strings.HasPrefix(l, "Service:") {
			filteredLabels = append(filteredLabels, l)
		}
	}
	return
}

func filterIssuesByLabel(label string, issues []*gitlab.Issue) (filteredIssues []*gitlab.Issue) {
	for _, p := range issues {
		if stringInSlice(label, p.Labels) {
			filteredIssues = append(filteredIssues, p)
		}
	}
	return
}

func filterIssuesByState(state string, issues []*gitlab.Issue) (filteredIssues []*gitlab.Issue) {
	for _, p := range issues {
		fmt.Println(state, p.Labels)
		if p.State == state {
			filteredIssues = append(filteredIssues, p)
		}
	}
	return
}

func choose(ss []string, test func(string) bool) (ret []string) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
