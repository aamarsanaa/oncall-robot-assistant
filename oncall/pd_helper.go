package oncall

import (
	"time"

	pagerduty "github.com/PagerDuty/go-pagerduty"
	config "gitlab.com/gl-infra/oncall-robot-assistant/config"
)

// GetOncallPersons returns the team members who were on call
// for the last 7 days for the given schedule
func getOncallPersons(config *config.Config, schedule string) []pagerduty.User {
	var options pagerduty.ListOnCallUsersOptions
	options.Since = nowPdDateWithOffset(-config.DayOffset)
	options.Until = nowPdDate()
	client := pagerduty.NewClient(config.APITokens.PagerDuty)
	if oncall, err := client.ListOnCallUsers(schedule, options); err != nil {
		panic(err)
	} else {
		return oncall
	}
}

func getOncallIncidents(config *config.Config) []pagerduty.Incident {
	client := pagerduty.NewClient(config.APITokens.PagerDuty)
	var opts pagerduty.ListIncidentsOptions
	opts.ServiceIDs = []string{config.PagerDuty.ServiceID}
	opts.Since = nowPdDateWithOffset(-config.DayOffset)
	var list pagerduty.APIListObject
	list.Limit = 100
	opts.APIListObject = list
	if incs, err := client.ListIncidents(opts); err != nil {
		panic(err)
	} else {
		return incs.Incidents
	}
}

func nowPdDate() string {
	return time.Now().UTC().Format("2006-01-02T15:04:05Z")
}

func nowPdDateWithOffset(offset int) string {
	return time.Now().UTC().AddDate(0, 0, offset).Format("2006-01-02T15:04:05Z")
}
