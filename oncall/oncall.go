package oncall

import (
	"bytes"
	"log"
	"strings"
	"text/template"
	"time"

	"github.com/GeertJohan/go.rice"
	gitlab "github.com/xanzy/go-gitlab"
	config "gitlab.com/gl-infra/oncall-robot-assistant/config"
)

// GraphData has information about a performance graph
type GraphData struct {
	Fname string
	Name  string
	URL   string
	Info  string
}

// ReportHelpers for clients needed for the report
type ReportHelpers struct {
	GitLab    *GitLabHelper
	GitLabDev *GitLabHelper
}

// Report clients and config for report
type Report struct {
	Config  *config.Config
	Helpers *ReportHelpers
}

// ReportOptions has options for the report
type ReportOptions struct {
}

//NewReport initializes the oncall report
func NewReport(config *config.Config, opts ReportOptions) *Report {
	r := Report{
		Config:  config,
		Helpers: newReportHelpers(config),
	}
	return &r
}

// CreateReportIssue - creates new report and returns an issue, this is a multi-step process
//  * Downloads graphs from grafana (these are specified in the yaml config)
//  * Uploads the graphs to the project
//  * Creates an issue using the issue template and returns the link
func (r *Report) CreateReportIssue(title string) string {
	// Not downloading performance graphs for now
	//   graphs := r.downloadGraphs()
	//   weeklyOpsGraphs := r.uploadGraphs(graphs)
	weeklyOpsGraphs := []WeeklyOpsGraph{}
	desc := r.generateTemplate(weeklyOpsGraphs)
	if r.Config.Debug == true {
		return desc
	}
	issue := r.Helpers.GitLab.CreateIssue(title, desc, r.Config.Projects.ReportProject.ID)
	return issue.WebURL
}

// Generate an issue using a template
func (r *Report) generateTemplate(weeklyOpsGraphs []WeeklyOpsGraph) string {

	// Fetch all issues opened during shift in both trackers
	// Infrastructure tracker
	log.Println("Fetching infrastructure issues")
	shiftIssues := r.Helpers.GitLab.GetIssuesOpenedDuringShift()
	allOpenIssues := r.Helpers.GitLab.GetIssuesOpenAll()

	// Production tracker
	log.Println("Fetching production issues")
	shiftIssuesProd := r.Helpers.GitLab.GetIssuesOpenedDuringShiftProd()
	allOpenIssuesProd := r.Helpers.GitLab.GetIssuesOpenAllProd()

	templateData := TemplateData{}

	// PagerDuty team members
	for _, schd := range r.Config.PagerDuty.Schedules {
		onCallUsers := getOncallPersons(r.Config, schd.ID)
		for _, u := range onCallUsers {
			otm := TeamMember{}
			otm.Schedule = schd.Name
			otm.User = u.Name
			templateData.TeamMembers = append(templateData.TeamMembers, otm)
		}
	}

	// PagerDuty incidents
	incidents := getOncallIncidents(r.Config)
	templateData.IncidentCount = len(incidents)
	for _, p := range incidents {
		inc := Incident{}
		inc.Summary = strings.Replace(p.Summary, "#", "", -1)
		inc.URL = p.HTMLURL
		inc.CreatedAt = p.CreatedAt
		templateData.Incidents = append(templateData.Incidents, inc)
	}

	// Open issue stats
	// Issue stats

	templateData.IssuesOpenAll.OnCall = len(filterIssuesByLabel("oncall", allOpenIssues))
	templateData.IssuesOpenAll.Change = len(filterIssuesByLabel("change", allOpenIssuesProd))
	templateData.IssuesOpenAll.Incident = len(filterIssuesByLabel("incident", allOpenIssuesProd))
	templateData.IssuesOpenAll.AccessRequest = len(filterIssuesByLabel("access request", allOpenIssues))
	templateData.IssuesOpenAll.CorrectiveAction = len(filterIssuesByLabel("corrective action", allOpenIssues))

	// Open incident issues
	for _, p := range filterIssuesByLabel("incident", allOpenIssuesProd) {
		templateData.OpenIncidentIssues = append(templateData.OpenIncidentIssues, *genIssueInfo(p))
	}

	// Open change issues
	for _, p := range filterIssuesByLabel("change", allOpenIssuesProd) {
		templateData.OpenChangeIssues = append(templateData.OpenChangeIssues, *genIssueInfo(p))
	}

	// Open oncall issues
	for _, p := range filterIssuesByLabel("oncall", allOpenIssues) {
		templateData.OpenOnCallIssues = append(templateData.OpenOnCallIssues, *genIssueInfo(p))
	}

	// 7 day issue stats

	templateData.IssuesOpenedDuringShift.OnCall = len(filterIssuesByLabel("oncall", shiftIssues))
	templateData.IssuesOpenedDuringShift.AccessRequest = len(filterIssuesByLabel("access request", shiftIssues))
	templateData.IssuesOpenedDuringShift.Change = len(filterIssuesByLabel("change", shiftIssuesProd))
	templateData.IssuesOpenedDuringShift.Incident = len(filterIssuesByLabel("incident", shiftIssuesProd))
	templateData.IssuesOpenedDuringShift.CorrectiveAction = len(filterIssuesByLabel("corrective action", shiftIssuesProd))

	// Shift incident issues
	for _, p := range filterIssuesByLabel("incident", shiftIssuesProd) {
		templateData.IncidentIssues = append(templateData.IncidentIssues, *genIssueInfo(p))
	}

	// Shift change issues
	for _, p := range filterIssuesByLabel("change", shiftIssuesProd) {
		templateData.ChangeIssues = append(templateData.ChangeIssues, *genIssueInfo(p))
	}

	// Shift corrective action issues
	for _, p := range filterIssuesByLabel("corrective action", shiftIssues) {
		templateData.CorrectiveActionIssues = append(templateData.CorrectiveActionIssues, *genIssueInfo(p))
	}

	templateBox, err := rice.FindBox("../templates")
	if err != nil {
		log.Fatal(err)
	}

	templateString, err := templateBox.String("on-call-report.tmpl")
	if err != nil {
		log.Fatal(err)
	}

	tmpl, err := template.New("message").Parse(templateString)
	if err != nil {
		log.Fatal(err)
	}
	var desc bytes.Buffer
	tmpl.Execute(&desc, templateData)
	return desc.String()
}

func genIssueInfo(p *gitlab.Issue) *Issue {
	issue := Issue{}
	created := p.CreatedAt.Format(time.RFC3339)
	if p.Assignee.Username == "" {
		p.Assignee.Username = "unassigned"
	}
	issue.CreatedAt = created
	issue.Summary = p.Title
	issue.URL = p.WebURL
	issue.Assignee = p.Assignee.Username
	issue.SeverityLabels = filterSeverityLabels(p.Labels)
	issue.ServiceLabels = filterServiceLabels(p.Labels)
	return &issue
}

func newReportHelpers(config *config.Config) *ReportHelpers {
	optsGitLab := GitLabHelperOptions{
		APIToken:      config.APITokens.GitLab,
		ProjectID:     config.Projects.Infrastructure.ID,
		ProjectIDProd: config.Projects.Production.ID,
		DayOffset:     config.DayOffset,
	}
	optsGitLabDev := GitLabHelperOptions{
		APIToken:      config.APITokens.GitLabDev,
		ProjectID:     config.Projects.Infrastructure.ID,
		ProjectIDProd: config.Projects.Production.ID,
		DayOffset:     config.DayOffset,
	}
	tc := ReportHelpers{
		GitLab:    NewGitLabHelper(optsGitLab),
		GitLabDev: NewGitLabHelper(optsGitLabDev),
	}
	return &tc
}

/********* NOT USED UNTIL WE ADD PERFORMANCE GRAPHS BACK TO REPORT
// Upload graphs to the gitlab project
func (r *Report) uploadGraphs(graphs <-chan GraphData) (opsGraphs []WeeklyOpsGraph) {
	log.Println("Uploading graphs to GitLab project ...")
	start := time.Now()
	for graph := range graphs {
		// defer os.Remove(graph.Fname)
		opsGraph := WeeklyOpsGraph{}
		log.Println("    uploading", graph.Name)
		pf := r.Helpers.GitLab.UploadFile(r.Config.Projects.ReportProject.ID, graph.Fname)
		opsGraph.URL = pf.URL
		opsGraph.Name = graph.Name
		opsGraphs = append(opsGraphs, opsGraph)
	}
	secs := time.Since(start).Seconds()
	log.Printf("    Completed upload in %.2f seconds\n", secs)
	return opsGraphs
}

// Downloads graphs from Grafana
func (r *Report) downloadGraphs() (graphs <-chan GraphData) {
	log.Println("Downloading graphs from performance ...")
	start := time.Now()
	var wg sync.WaitGroup
	numGraphs := len(weeklyOpsGraphs(r.Config))
	ch := make(chan GraphData, numGraphs)
	wg.Add(numGraphs)
	for _, graph := range weeklyOpsGraphs(r.Config) {
		go makeRequest(graph.URL, graph.Name, ch, &wg, r.Config.APITokens.Grafana)
	}
	wg.Wait()
	close(ch)
	secs := time.Since(start).Seconds()
	log.Printf("    %d graphs took %.2f seconds to download\n", numGraphs, secs)
	return ch
}

func makeRequest(url string, name string, ch chan<- GraphData, wg *sync.WaitGroup, grafanaKey string) {
	defer wg.Done()
	start := time.Now()
	file, _ := ioutil.TempFile(os.TempDir(), "oncall-graph")
	graphData := GraphData{}
	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Authorization", "Bearer "+grafanaKey)
	resp, err := client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		log.Fatalf("Unable to request graphs from grafana: %s", err.Error())
	}
	secs := time.Since(start).Seconds()
	body, _ := ioutil.ReadAll(resp.Body)
	err = ioutil.WriteFile(file.Name(), body, 0644)
	if err != nil {
		log.Fatalf("Unable to write graph images: %s", err.Error())
	}
	graphData.Name = name
	graphData.URL = url
	graphData.Fname = file.Name()
	graphData.Info = fmt.Sprintf("%.2f elapsed with response length: %d %s", secs, len(body), url)
	ch <- graphData
}

func weeklyOpsGraphs(config *config.Config) (graphs []WeeklyOpsGraph) {
	for _, graph := range config.WeeklyOps {
		opsGraph := WeeklyOpsGraph{}
		opsGraph.Name = graph.Name
		millisTo := time.Now().UnixNano() / 1000000
		millisFrom := millisTo - (86400000 * int64(config.DayOffset))
		opsGraph.URL = graph.URL + fmt.Sprintf("&from=%d&to=%d", millisFrom, millisTo)
		graphs = append(graphs, opsGraph)
	}
	return graphs
}
**********************/
