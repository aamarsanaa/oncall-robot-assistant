# Oncall Robot Assistant

This is a utility for helping to automate on-call reporting
tasks. It's main purpose to help generate the weekly reports.

# How to use

* `go get -d gitlab.com/gl-infra/oncall-robot-assistant`
* Install dep https://github.com/golang/dep#setup
* `dep init`
* `go build`
* `go install`
* Copy `oncall-settings-example.yaml` to `~/.oncall-settings.yaml` and update the pagerduty, grafana and gitlab tokens. These are located in 1Password in the production vault, grep for `oncall-robot`.
* Run `oncall-robot-assistant` to generate the report.

This will create an issue link, modify the issue as you see fit
