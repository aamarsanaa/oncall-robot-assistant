package config

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// WeeklyOps for performance graphs
type WeeklyOps struct {
	Name string `yaml:"name"`
	URL  string `yaml:"url"`
}

// PagerDutySchedule for pd rotations
type PagerDutySchedule struct {
	Name string `yaml:"name"`
	ID   string `yaml:"id"`
}

// PagerDuty for PD schedules
type PagerDuty struct {
	ServiceID         string              `yaml:"service_id"`
	Schedules         []PagerDutySchedule `yaml:"schedules"`
	PrimarySchedule   string              `yaml:"primary"`
	SecondarySchedule string              `yaml:"secondary"`
}

// Project has data for a GitLab project
type Project struct {
	ID int `yaml:"id"`
}

// Projects for project ids
type Projects struct {
	Infrastructure Project `yaml:"infrastructure"`
	Production     Project `yaml:"production"`
	ReportProject  Project `yaml:"report_project"`
}

// APITokens for api tokens
type APITokens struct {
	GitLab    string `yaml:"gitlab"`
	GitLabDev string `yaml:"gitlab_dev"`
	Grafana   string `yaml:"grafana"`
	PagerDuty string `yaml:"pager_duty"`
}

// Config for holding report settings
type Config struct {
	APITokens APITokens `yaml:"api_tokens"`
	Projects  Projects  `yaml:"projects"`

	DayOffset   int         `yaml:"day_offset"`
	OncallLabel string      `yaml:"oncall_label"`
	PagerDuty   PagerDuty   `yaml:"pagerduty"`
	WeeklyOps   []WeeklyOps `yaml:"weekly_ops"`
	Debug       bool
}

// ParseConfig parses the yaml configuration from a reader
func ParseConfig(r io.Reader) (Config, error) {
	configBytes, err := ioutil.ReadAll(r)
	config := Config{}
	if err != nil {
		return config, fmt.Errorf("could not read configuration: %s", err)
	}
	err = yaml.Unmarshal(configBytes, &config)
	if err != nil {
		return config, fmt.Errorf("could not parse configuration yaml: %s", err)
	}
	return config, nil
}

// ReadConfig will attempt to read the different configuration
// parameters from a yaml formatted file.
func ReadConfig(f string, d bool) (*Config, error) {
	var cfg Config
	if _, err := os.Stat(f); os.IsNotExist(err) {
		return nil, errors.New(err.Error())
	}
	content, err := ioutil.ReadFile(f)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	err = yaml.Unmarshal(content, &cfg)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	cfg.Debug = d
	return &cfg, nil
}
